# Présentation 📢

Le hackathon [OwnTech](https://www.owntech.org/) est un marathon de programmation 💻😅, il est basé sur cet outil open-source pour contrôler la puissance électrique ⚡

Venez _Créez, Apprenez et Innovez_ avec un groupe de passionnés ;-)

## C'est Quoi un Hackathon !

Durant cette journée libre et gratuite, vous pourrez nous rejoindre afin de partager un moment de découverte et de travail collaborative sur des projets de programmation informatique autour de la carte OwnTech.

Il n'y a aucune obligation, vous pouvez simplement venir nous voir pour discuter 💬 5 min ou rester un peu plus longtemps 🏉 si le coeur vous en dit ❤️, pas de prérequis, l'idée est avant tout d'apprendre en s'amusant 🤩

https://fr.wikipedia.org/wiki/Hackathon

https://klaxoon.com/communaute/organiser-un-hackathon-faites-emerger-des-solutions-creatives-en-stimulant-esprit-de-competition-et-emulation

## 💡Pour Quoi faire ?

Cela peut servir pour des systèmes à énergie solaire, des chargeurs de batterie, des alimentations de laboratoire, des contrôleurs de moteur et bien plus encore...

Mais avant tous, cela nous permettra d'apprendre 🔬 plein de trucs sympa, comme la programmation des STM32 et surtout comprendre comment fonctionne un convertisseur d'énergie ✨

## 📌 Mais ça ce passe Où ?

Retrouvez nous ici 🗺️ au [FabRiquet](https://fabriquet.fr/acces/)

## ⏰ Et c'est pour Quand ?

Cet événement aura lieu le samedi 7 mai 2022, de 9h à 19h.

Attention ⚠️, à 18h on lance 🏉 un apéro façon auberge espagnole 🍻

## Comment ?

### 📖 Tutoriel Clignotant à led ou Blink led

Lors de cette journée nous commencerons par une présentation et une prise en main des outils :

https://www.owntech.org/en/blinky-tutorial/

## 👥 Auteurs et remerciements

Montrez votre appréciation à ceux qui ont contribué à ce projet :

- [x] +1 pour Luiz 👋,
- [x] Arnauld pour le template du hackathon FabTronic,
- [x] Guillaume est bien partie 🪛 🔨 🛠,
- [ ] A vous de jouer...

## Licence

open source...
